import argparse
import random
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument("nbImm", type=int, help='Number of immigrants')
parser.add_argument("initA", type=int, help='Initial population of A')
parser.add_argument("initB", type=int, help='Initial population of B')
parser.add_argument("nbExp", type=int, help='Number of experiments')
args = parser.parse_args()

nbImm = args.nbImm
A = args.initA
B = args.initB
nbExp = args.nbExp
m = []

for j in range(nbExp):
    A = args.initA
    B = args.initB
    for i in range(nbImm):
        if random.random() < A/(A+B):
            A+=1
        else:
            B+=1

    m.append(A/(A+B))

plt.hist(m,30)
plt.title("Immigration")
plt.xlabel("Limit")
plt.ylabel("Frequency")
plt.show()
